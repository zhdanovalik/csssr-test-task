import svg4everybody from 'svg4everybody';
import $ from 'jquery';

$(() => {
	svg4everybody();

	// on radio button click change visual part on "checked" element
	$('.form__radio-item').click(function () {
		var t = $(this);
		if (t.parent().hasClass('form__radio-label_checked')) {
			return;
		}
		$('.form__radio .form__radio-label_checked').removeClass('form__radio-label_checked');
		t.parent().addClass('form__radio-label_checked');
	});

	// on checkbox click toggle visual part of "checked" property
	$('.form__checkboxes-label-item').click(function () {
		$(this).parent().toggleClass('form__checkboxes-label_checked');
	});

	var clicking = false;

	// check whether it's clicked on thumb or not
	$(document).mousedown(function (e) {
		if (e.target === $('.form__slider-item-thumb')[0]) {
			clicking = true;
		}
	});
	$(document).mouseup(function (e) {
		clicking = false;
	});

	// slide thumb
	$(document).mousemove(function (event) {
		if (clicking) {
			var sliderMin = ($('.form__slider-item').offset().left - 5);
			var sliderMax = sliderMin + 750;
			var mousePos = event.pageX;
			var newPos = mousePos - sliderMin + 'px';
			if (sliderMin <= mousePos && mousePos <= sliderMax) {
				$('.form__slider-item-thumb').css('left', newPos);
			} else if (sliderMin >= mousePos) {
				$('.form__slider-item-thumb').css('left', -5);
			} else {
				$('.form__slider-item-thumb').css('left', 750);
			}
		}
	});
});
